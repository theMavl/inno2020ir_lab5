db.createUser(
        {
            user: "user",
            pwd: "123456",
            roles: [
                {
                    role: "readWrite",
                    db: "ir_index"
                }
            ]
        }
);

db.createCollection( "songs", {
   validator: { $jsonSchema: {
      bsonType: "object",
      required: [ "title", "artist", "text", "url", "status" ],
      properties: {
         title: {
            bsonType: "string",
            description: "must be a string and is required"
         },
         artist: {
            bsonType: "string",
            description: "must be a string and is required"
         },
         text: {
            bsonType: "string",
            description: "must be a string and is required"
         },
         url: {
            bsonType: "string",
            description: "must be a string and is required"
         },
         status: {
            enum: [ "new", "indexed", "deleted" ],
            description: "must be a enum and is required"
         }
      }
   } }
} );

db.createCollection( "inverted_index", {
   validator: { $jsonSchema: {
      bsonType: "object",
      required: [ "token"],
      properties: {
         token: {
            bsonType: "string",
            description: "must be a string and is required"
         },
         songs: {
            bsonType: "array",
            description: "must be an array"
         }
      }
   } }
} );

db.inverted_index.createIndex( { "token": 1 }, { unique: true } );
db.songs.createIndex( { "title": 1, "artist": 1 }, { unique: true } );