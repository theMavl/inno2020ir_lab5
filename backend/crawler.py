import hashlib
import logging
import os
import pickle
import re
import sys
import urllib.parse
from collections import Counter
from string import punctuation
from urllib.parse import quote

import requests
from bs4 import BeautifulSoup
from bs4.element import Comment
from pymongo import MongoClient, errors
from sentence_splitter import SentenceSplitter

SONGS_PATH = "./index/songs.pickle"
CACHE_STORE_PATH = "./index/store/"


class Song:
    def __init__(self, title, artist, text, url):
        self.title = title
        self.artist = artist
        self.text = text
        self.url = url

    def __repr__(self):
        return '"%s" (by %s) - %s ' % (self.title, self.artist, self.url)


class Document:

    def __init__(self, url):
        self.url = url
        self.urlhash = hashlib.md5(self.url.encode('utf-8')).hexdigest()
        self.anchors = []
        self.images = []
        self.is_song = False
        self.song = None
        self.content = None

    def get(self, force=False):
        if force:
            if not self.download():
                self.content = ""
                print("Error while downloading", self.url)
                return
        else:
            if not self.load():
                if not self.download():
                    self.content = ""
                    print("Error while downloading", self.url)
                    return
                else:
                    self.persist()

    def download(self):
        try:
            r = requests.get(self.url, timeout=10)
            if not r:
                return False
            self.content = r.content
            return True
        except Exception:
            return False

    def persist(self):
        with open(CACHE_STORE_PATH + self.urlhash, "wb+") as file:
            file.write(self.content)

    def load(self):
        if os.path.exists(CACHE_STORE_PATH + self.urlhash):
            with open(CACHE_STORE_PATH + self.urlhash, "rb") as file:
                self.content = file.read()

            return True
        else:
            return False


class HtmlDocument(Document):

    @staticmethod
    def tag_visible(element):
        if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
            return False
        if isinstance(element, Comment):
            return False
        return True

    def parse(self):
        soup = BeautifulSoup(self.content, 'html.parser')

        f_title = soup.find(id="lyric-title-text")
        f_artist = soup.find("h3", {"class": "lyric-artist"})
        f_text = soup.find(id="lyric-body-text")

        if f_title and f_artist and f_text:
            self.is_song = True
            self.song = Song(f_title.text, f_artist.a.text, f_text.text, self.url)

        for link in soup.find_all('a'):
            href = link.get('href')
            abs_url = urllib.parse.urljoin(self.url, href)

            if href == "javascript:;":
                continue

            if not re.match(r'.*(lyrics\.com\/).*', abs_url) or re.match(r'.*(\.php).*', abs_url):
                continue

            if len(link.contents) > 0:
                link_text = link.contents[0]
            else:
                link_text = ""

            self.anchors.append((link_text, abs_url))

        texts = soup.findAll(text=True)
        visible_texts = filter(self.tag_visible, texts)
        self.text = u" ".join(t.strip() for t in visible_texts)


class HtmlDocumentTextData:

    def __init__(self, url, force=False):
        self.doc = HtmlDocument(url)
        if url[-4:] not in ('.pdf', '.mp3', '.avi', '.mp4', '.txt'):
            self.doc.get(force)
            self.doc.parse()

    def get_sentences(self):
        splitter = SentenceSplitter()
        result = splitter.split(text=self.doc.text)
        return result

    def get_word_stats(self):
        a = list(filter(None, (word.lower().strip(punctuation) for word in self.doc.text.split())))
        return Counter(a)


class Crawler:

    @staticmethod
    def crawl_generator(source, processed_urls, depth=1):
        if depth == 0:
            pass
        else:
            logging.info("Yield url: %s, src: %s, cd: %d" % (source, "None", depth))

            root = HtmlDocumentTextData(source)
            yield root

            for text, url in root.doc.anchors:
                if url in processed_urls:
                    logging.info("[2] Already seen: %s, src: %s, cd: %d" % (url, "None", depth))
                    continue
                else:
                    logging.info("Yield url: %s, src: %s, cd: %d" % (url, source, depth))

                yield HtmlDocumentTextData(url)
                yield from Crawler.crawl_generator(url, processed_urls, depth - 1)

    @staticmethod
    def get_collection(start_url):

        if os.path.exists(SONGS_PATH):
            with open(SONGS_PATH, 'rb') as f:
                return pickle.load(f)

        processed_urls = set()

        found_songs = {}
        songs = []
        sc = 0

        hash_url_mapping = {}

        for c in Crawler.crawl_generator(start_url, processed_urls, 2):
            logging.info(c.doc.url)
            if c.doc.urlhash in hash_url_mapping:
                logging.info("Collision!", c.doc.url, hash_url_mapping[c.doc.urlhash])
                pass
            else:
                hash_url_mapping.update({c.doc.urlhash: c.doc.url})

            processed_urls.add(c.doc.url)
            if c.doc.is_song:
                song_signature = c.doc.song.title + " " + c.doc.song.artist
                if song_signature not in found_songs:
                    songs.append(c.doc.song)
                    found_songs.update({song_signature: c.doc.song})
                    logging.info("[%d] %s" % (sc, c.doc.song))
                    sc += 1
                    if sc == 1000:
                        print("Got 1000 songs, exiting...")
                        break

        print("Done")
        with open(SONGS_PATH, 'wb+') as f:
            pickle.dump(songs, f)

        return songs

    @staticmethod
    def collection_to_mongo(collection):
        client = MongoClient('mongodb://admin:123456@10.90.138.181:27017/')
        db = client['ir_index']
        songs = db.songs
        try:
            doc_ins = []
            for x in collection:
                tmp = x.__dict__
                tmp['status'] = 'new'
                doc_ins.append(tmp)

            new_result = songs.insert_many(doc_ins)
            print('Inserted: {0}'.format(new_result.inserted_ids))
        except errors.BulkWriteError as e:
            print(e.details)
