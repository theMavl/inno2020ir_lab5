from text_process import normalize, tokenize, lemmatization, lemmatize, remove_stop_word
from crawler import Song
from Levenshtein import distance
import jellyfish
from typing import List
from pymongo import MongoClient
from crawler import HtmlDocumentTextData

# TODO: оптимизировать префиксное дерево:
#   - не хранить ссылки на песни,
#   - строить по лемматизированным термам
#   - Найденное слово отправлять в инвертированный индекс

client = MongoClient('mongodb://admin:123456@10.90.138.181:27017/')
db = client['ir_index']
songs = db.songs
index_db = db.inverted_index


class TrieNode(object):
    def __init__(self, char: str):
        self.char = char
        self.children = []
        self.word_finished = False
        self.word = None
        self.counter = 1

    def add(self, token: str, word: str) -> None:
        node = self
        for char in token:
            found_in_child = False
            for child in node.children:
                if child.char == char:
                    child.counter += 1
                    node = child
                    found_in_child = True
                    break
            if not found_in_child:
                new_node = TrieNode(char)
                node.children.append(new_node)
                node = new_node
        node.word_finished = True
        node.word = word

    def find_node(self, prefix: str):
        node = self
        if not node.children:
            return None
        for char in prefix:
            char_not_found = True
            for child in node.children:
                if child.char == char:
                    char_not_found = False
                    node = child
                    break
            if char_not_found:
                return None
        return node

    def find_prefix(self, prefix: str) -> List[str]:
        node = self.find_node(prefix)
        if not node:
            return []
        traversed = node.traverse()
        print(traversed)
        return [x.word for x in traversed]

    def traverse(self) -> list:
        nodes = []
        if self.word_finished:
            nodes.append(self)
        for c in self.children:
            for n in c.traverse():
                nodes.append(n)
        return nodes


class Index(object):
    def __init__(self, mongo=False, collection=None):
        self.index = None
        self.mongo_index = False
        self.soundex = {}
        self.prefix_tree = TrieNode('*')
        self.songs_n = None
        if mongo:
            self.update_mongo_index(init=True)
        elif collection:
            self.make_index(collection)
        else:
            raise Exception("No collection provided and not in Mongo.")

    def update_mongo_index(self, init=False):
        if init:
            cursor = songs.find({})
            self.songs_n = cursor.count()
        else:
            cursor = songs.find({'status': 'new'})
            self.songs_n += cursor.count()
            print("Found {} new songs.".format(cursor.count()))

        for song in cursor:
            print(song)
            prepr_tree = set(remove_stop_word(tokenize(normalize(song['text']))))
            preprocessed = set(lemmatization(prepr_tree))
            for word in preprocessed:
                if song['status'] == 'new':
                    index_db.update(
                        {'token': word},
                        {'$addToSet': {'songs': song['_id']}}, upsert=True
                    )

                if init or (not init and song['status'] == 'new'):
                    sndx = jellyfish.soundex(word)
                    if sndx in self.soundex:
                        self.soundex[sndx].add(word)
                    else:
                        self.soundex.update({sndx: {word}})

                    # for word in prepr_tree:
                    word_e = word + '$'

                    for i in range(len(word) + 1):
                        neww = word_e[-i:] + word_e[:-i]
                        trie_node = self.prefix_tree.find_node(neww)
                        if not trie_node or not trie_node.word_finished:
                            self.prefix_tree.add(neww, word)

            if song['status'] == 'new':
                songs.update({'_id': song['_id']},
                             {"$set": {
                                 'status': 'indexed'
                             }
                             },
                             upsert=False)

        if init:
            self.mongo_index = True

    def _make_index(self, collection):
        """
        Deprecated.
        """
        inverted_index = {}
        soundex_index = {}
        self.songs_n = len(collection)
        prefix_tree = TrieNode('*')

        for song in collection:
            prepr_tree = set(remove_stop_word(tokenize(normalize(song.text))))
            preprocessed = set(lemmatization(prepr_tree))
            for word in preprocessed:
                if word in inverted_index:
                    inverted_index[word].add(song)
                else:
                    inverted_index.update({word: {song}})

                sndx = jellyfish.soundex(word)
                if sndx in soundex_index:
                    soundex_index[sndx].add(word)
                else:
                    soundex_index.update({sndx: {word}})

                # for word in prepr_tree:
                word_e = word + '$'

                for i in range(len(word) + 1):
                    neww = word_e[-i:] + word_e[:-i]

                    trie_node = prefix_tree.find_node(neww)
                    if not trie_node:
                        prefix_tree.add(neww, word)

        self.index = inverted_index
        self.soundex = soundex_index
        self.prefix_tree = prefix_tree

    @staticmethod
    def verify_collection() -> int:
        """
        Check every song's availability. If something wrong, remove from inverted index and mark as trash
        :return: number of trash songs
        """
        cursor = songs.find({})
        trash_count = 0
        for song in cursor:
            trash = False
            song_page = HtmlDocumentTextData(song['url'], force=True)
            if song_page.doc.content == "":
                trash = True
            elif song_page.doc.is_song:
                if song_page.doc.song.artist != song['artist'] or song_page.doc.song.title != song['title']:
                    trash = True
            else:
                trash = True

            if trash:
                trash_count += 1
                songs.update({'_id': song['_id']},
                             {"$set": {'status': 'deleted'}},
                             upsert=False)
                tokens = index_db.find({'songs': song['_id']})
                for token in tokens:
                    songs.update({'_id': token['_id']},
                                 {'$pull': {'songs': song['_id']}}
                                 )
        return trash_count

    @staticmethod
    def collect_trash():
        songs.delete_many({'status': 'deleted'})

    def search(self, query):
        if not self.index and not self.mongo_index:
            raise Exception("Index is not built!")

        p_query = remove_stop_word(tokenize(normalize(query, wildcard_allowed=True)))
        relevant_documents = None

        for word in p_query:
            word_lem = lemmatize(word)
            cursor = index_db.find({'token': word_lem})
            print("Looking for {} ({} found)".format(word_lem, cursor.count()))

            # if word_lem not in self.index or len(self.index[word_lem]) == 1:
            if cursor.count() == 0 or (cursor.count() == 1 and len(cursor[0]['songs']) == 1):
                # Word is not in lemmatized index
                tmp_relevant_docs = None
                if '*' in word:
                    if word.count('*') > 1:
                        return []

                    trie_query = word.split('*')
                    trie_query = trie_query[1] + '$' + trie_query[0]
                    wildcard_words = self.prefix_tree.find_prefix(trie_query)
                    print(trie_query, wildcard_words)

                    for w in wildcard_words:
                        token = index_db.find_one({'token': w})
                        if not token:
                            continue

                        new_results = set(token['songs'])
                        if tmp_relevant_docs:
                            tmp_relevant_docs = tmp_relevant_docs.union(new_results)
                        else:
                            tmp_relevant_docs = new_results

                    if relevant_documents:
                        relevant_documents = relevant_documents.intersection(tmp_relevant_docs)
                    else:
                        relevant_documents = tmp_relevant_docs
                else:
                    # Soundex
                    sndx = jellyfish.soundex(word)
                    if sndx not in self.soundex:
                        continue
                    soundex_suggestions = self.soundex[sndx]
                    soundex_filtered = {}
                    min_dst = 99999999
                    for w in soundex_suggestions:
                        d = distance(word, w)
                        if d < min_dst:
                            min_dst = d

                        if d not in soundex_filtered:
                            soundex_filtered.update({d: {w}})
                        else:
                            soundex_filtered[d].add(w)

                    if min_dst == 0 and (cursor.count() == 1 and len(cursor[0]['songs']) == 1):
                        # Another fucking-french-song-that-breaks-the-index detected. Ignore that crap
                        soundex_filtered.pop(0)
                        min_dst = min(soundex_filtered.keys())

                    predicted_words = soundex_filtered[min_dst]
                    print("Predicted words:", predicted_words)
                    for p_word in predicted_words:
                        token = index_db.find_one({'token': p_word})
                        song_obj_ids = token['songs']
                        new_results = set(song_obj_ids)
                        # new_results = self.index[p_word]

                        if tmp_relevant_docs:
                            tmp_relevant_docs = tmp_relevant_docs.union(new_results)
                        else:
                            tmp_relevant_docs = new_results

                    if relevant_documents:
                        relevant_documents = relevant_documents.intersection(tmp_relevant_docs)
                    else:
                        relevant_documents = tmp_relevant_docs
            else:
                # Word is in lemmatized index
                # new_results = self.index[word_lem]
                song_obj_ids = cursor[0]['songs']
                new_results = set(song_obj_ids)
                if relevant_documents:
                    relevant_documents = relevant_documents.intersection(new_results)
                else:
                    relevant_documents = new_results
        if relevant_documents:
            relevant_documents = list(relevant_documents)
        else:
            relevant_documents = []

        relevant_documents_ret = []
        for i in relevant_documents:
            song = songs.find_one({'_id': i, 'status': 'indexed'})
            print(song)
            relevant_documents_ret.append(song)
        print(relevant_documents_ret)
        return relevant_documents_ret
