import re
import string
import nltk
import unidecode
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet as wn
from collections import defaultdict

nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
nltk.download('stopwords')
nltk.download('punkt')


# normilize text
def normalize(text, wildcard_allowed=False):
    result = text.lower()
    result = re.sub(r'\d\*+', '', result)
    result = unidecode.unidecode(result)
    punct_list = string.punctuation if not wildcard_allowed else string.punctuation.replace('*', '')
    result = result.translate(str.maketrans('', '', punct_list))
    result = result.strip()
    return result


# tokenize text using nltk lib
def tokenize(text):
    return nltk.word_tokenize(text)


def lemmatization(tokens):
    tag_map = defaultdict(lambda: wn.NOUN)
    tag_map['J'] = wn.ADJ
    tag_map['V'] = wn.VERB
    tag_map['R'] = wn.ADV

    lemmatizer = WordNetLemmatizer()
    tags = nltk.pos_tag(tokens)
    return [lemmatizer.lemmatize(word, pos=tag_map[tag[0]]) for word, tag in tags]


def lemmatize(word):
    return lemmatization([word])[0]


def remove_stop_word(tokens):
    stop_words = set(stopwords.words('english'))
    return [word for word in tokens if word not in stop_words]


def preprocess(text):
    return remove_stop_word(lemmatization(tokenize(normalize(text))))
