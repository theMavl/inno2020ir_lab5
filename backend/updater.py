import logging
import sys
import time

from pymongo import MongoClient

from crawler import Crawler
from indexing import Index

client = MongoClient('mongodb://admin:123456@10.90.138.181:27017/')
db = client['ir_index']
songs = db.songs
index_db = db.inverted_index

logging.basicConfig(filename='crawler.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


def continuous_indexing(index: Index):
    print("Continuous indexing initiated.")
    while True:
        time.sleep(60)
        print("Task: Update Mongo Index")
        index.update_mongo_index()


def continuous_verifying(index: Index):
    print("Continuous verifying initiated.")
    while True:
        time.sleep(600)
        print("Task: Verify Collection")
        trash_c = index.verify_collection()
        print("{} documents trashed".format(trash_c))
        if trash_c > 0:
            index.collect_trash()


def continuous_crawling(start_url):
    print("Continuous crawling initiated.")
    processed_urls = set()
    hash_url_mapping = {}

    for c in Crawler.crawl_generator(start_url, processed_urls, 100):
        logging.info(c.doc.url)
        if c.doc.urlhash in hash_url_mapping:
            logging.info("Collision! %s %s".format(c.doc.url, hash_url_mapping[c.doc.urlhash]))
            pass
        else:
            hash_url_mapping.update({c.doc.urlhash: c.doc.url})

        processed_urls.add(c.doc.url)
        if c.doc.is_song:
            s = songs.find_one({'title': c.doc.song.title, 'artist': c.doc.song.artist})
            if not s:
                print("New song:", c.doc.song)
                tmp = c.doc.song.__dict__
                tmp['status'] = 'new'
                songs.insert(tmp)


if __name__ == "__main__":
    continuous_crawling("https://www.lyrics.com/")
    pass
