from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap
from indexing import Index
from crawler import Crawler
from threading import Thread
import time
from pymongo import MongoClient
from updater import continuous_crawling, continuous_indexing, continuous_verifying

app = Flask(__name__)
index = None
continuous_indexing_thread = None
client = MongoClient('mongodb://admin:123456@10.90.138.181:27017/')
db = client['ir_index']
songs = db.songs


@app.route('/')
def hello_world():
    if index:
        songs_n = songs.estimated_document_count()
        return render_template('search.html', songs_n=songs_n)
    else:
        return render_template('search.html')


@app.route('/search', methods=['GET'])
def search():
    if not index:
        return render_template('search.html')

    start = time.time()
    query = request.args.get('query')
    relevant = index.search(query)
    end = time.time()
    # dicts = [vars(r) for r in relevant]
    if query:
        return render_template('results.html', query=query, results=relevant, results_n=len(relevant),
                               time_elapsed="%.4f" % (end - start))
    else:
        return render_template('search.html')


def build_index(collection, is_mongo):
    global index, continuous_indexing_thread
    print("Indexing...")
    if is_mongo:
        index = Index(mongo=True)
    else:
        index = Index(mongo=False, collection=collection)
    print("Indexing complete.")
    continuous_indexing_thread = Thread(target=continuous_indexing, args=[index])
    continuous_indexing_thread.start()

    continuous_verifying_thread = Thread(target=continuous_verifying, args=[index])
    continuous_verifying_thread.start()


if __name__ == "__main__":
    from crawler import Song

    # collection = Crawler.get_collection("https://www.lyrics.com/")
    # Crawler.collection_to_mongo(collection)
    # Index.make_mongo_index()

    init_indexing_thread = Thread(target=build_index, args=[None, True])
    init_indexing_thread.start()

    continuous_crawling_thread = Thread(target=continuous_crawling, args=["https://www.lyrics.com/"])
    continuous_crawling_thread.start()

    bootstrap = Bootstrap(app)
    app.run(debug=True, host='0.0.0.0', use_reloader=False)
